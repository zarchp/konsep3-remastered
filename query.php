<?php
session_start();
include("koneksi.php");

$lokasi_file = $_FILES['image']['tmp_name'];
$dir = "images/";
$nama_file = $_FILES['image']['name'];
$ukuran = $_FILES['image']['filesize'];

move_uploaded_file($lokasi_file, $dir . $nama_file);

switch ($_GET['page']) {

	case "add_admin":
		$pass = $_POST['password'];
		$pass = md5($pass);
		mysqli_query($con, "insert into admin(username,password,level,nick)
		values('$_POST[username]','$pass','$_POST[level]','$_POST[nick]')");
		header('location:main.php?page=admin');
		break;

	case "cek":
		function anti_injection($con, $data)
		{
			$filter = mysqli_real_escape_string($con, stripslashes(strip_tags(htmlspecialchars($data, ENT_QUOTES))));
			return $filter;
		}

		$user = anti_injection($con, $_POST['username']);
		$pass = $_POST['password'];
		$pass = md5($pass);
		$pass = anti_injection($con, $pass);
		//$pass = anti_injection(md5($_POST['password']));

		if (!ctype_alnum($user) or !ctype_alnum($pass)) {
			header('location:main.php?page=admin&fail');
		} else {
			$login = mysqli_query($con, "select * from admin where username='$user' AND password='$pass'");
			$ketemu = mysqli_num_rows($login);
			$r = mysqli_fetch_array($login);
			if ($ketemu == 1) {
				// session_register("namauser");
				// session_register("level");

				$_SESSION['namauser'] = $r['username'];
				$_SESSION['level'] = $r['level'];

				header('location:main.php?page=admin');
			} else {
				header('location:main.php?page=admin&fail');
			}
		}
		break;

	case "add":
		mysqli_query($con, "insert into artikel(id_artikel,judul,isi,image)
		values('$_POST[id_artikel]','$_POST[judul]','$_POST[isi]','$nama_file')");
		header('location:main.php?page=admin&add');
		break;

	case "delete":
		mysqli_query($con, "delete from artikel where id_artikel=$_GET[id_artikel];");
		header('location:main.php?page=admin&delete');
		break;

	case "edit":
		mysqli_query($con, "update artikel set judul='$_POST[judul]', isi='$_POST[isi]', image='$nama_file' where id_artikel='$_GET[id_artikel]'");
		header('location:main.php?page=admin&update');
		break;

	case "add_komentar":
		mysqli_query($con, "insert into komentar(id_artikel,nama_komentar,email_komentar,isi_komentar)
		values('$_POST[id_artikel]','$_POST[nama_komentar]','$_POST[email_komentar]','$_POST[isi_komentar]')");
		header('location:main.php?page=artikel&id_artikel=' . $_POST['id_artikel']);
		break;

	case "delete_komentar":
		mysqli_query($con, "delete from komentar where id_komentar=$_GET[id_komentar];");
		header('location:main.php?page=artikel&id_artikel=' . $_GET['id_artikel']);
		break;

	case "simpan_polling":
		mysqli_query($con, "insert into polling(judul_poll,deskripsi)
		values('$_POST[judul_poll]','$_POST[deskripsi]')");
		$tampil = mysqli_query($con, "select * from polling");
		while ($t = mysqli_fetch_array($tampil)) {
			header('location:main.php?page=adddetailpoll&id_poll=' . $t['id_poll']);
		}
		break;

	case "simpan_det_polling":
		mysqli_query($con, "insert into detail_polling(id_poll,isi_detail)
		values('$_POST[id_poll]','$_POST[isi_detail]')");
		header('location:main.php?page=adddetailpoll&id_poll=' . $_GET['id_poll'] . '&isi_detail=$_POST[isi_detail]');
		break;

	case "deletepoll":
		mysqli_query($con, "delete from polling where id_poll=$_GET[id_poll];");
		header('location:main.php?page=admin&deletepoll');
		break;

	case "editpoll":
		mysqli_query($con, "update polling set judul_poll='$_POST[judul_poll]', deskripsi='$_POST[deskripsi]' where id_poll='$_GET[id_poll]'");
		header('location:main.php?page=admin&updatepoll');
		break;

	case "deletedetpoll":
		mysqli_query($con, "delete from detail_polling where id_detail=$_GET[id_detail];");
		header('location:main.php?page=editpoll&deletedetpoll');
		break;

	case "editdetpoll":
		mysqli_query($con, "update detail_polling set isi_detail='$_POST[isi_detail]' where id_detail='$_GET[id_detail]'");
		header('location:main.php?page=editpoll&updatedetpoll');
		break;

	case "vote":
		$tampil = mysqli_query($con, "select * from detail_polling where isi_detail='$_POST[pilih]'");
		$d = mysqli_fetch_array($tampil);
		$vote = $d['pemilih'] + 1;
		mysqli_query($con, "update detail_polling set pemilih='$vote' where isi_detail='$_POST[pilih]'");
		header('location:main.php?page=home');
		break;

	case "search":
		header('location:main.php?page=search&q=' . $_POST['q']);
		break;
}
