-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 23, 2012 at 01:23 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `konsep3`
--
CREATE DATABASE `konsep3` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `konsep3`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `username` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` varchar(10) DEFAULT NULL,
  `nick` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`, `level`, `nick`) VALUES
('armando', 'f4a8814674a6eddf1e61b1b67e1e62bf', '90', 'kaka'),
('zarchp', 'cd98aa19c7290cfe35818250d138a260', 'admin', 'ZarChp');

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE IF NOT EXISTS `artikel` (
  `id_artikel` varchar(5) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `isi` text,
  `dibaca` int(10) DEFAULT '0',
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `image` text,
  `kategori` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_artikel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `judul`, `isi`, `dibaca`, `tanggal`, `image`, `kategori`) VALUES
('1', 'DotA 6.74 AI', '<p>This information come from PBMN. Semua orang pasti sedang menunggu map Ai terbaru segera diluncurkan, karena sudah terlalu lama map AI ini ditunggu dan masih juga belum diluncurkan. PBMN baru saja menginformasikan bahwa ia tidak jadi membuat map 6.73c AI.</p>\r\n<p>Hal tersebut terjadi dikarenakan map 6.74 sudah diluncurkan terlebih dahulu, jadi PBMN memutuskan untuk menghentikan pembuatan map 6.73c AI. Tetapi PBMN akan langsung mengerjakan map 6.74 AI. Kenapa PBMN langsung membuat 6.74 AI dan tidak melanjutkan map 6.73c AI ? Kalian pasti sudah tau jawabannya, karena sudah jelas Dota lovers lebih senang memainkan map terbaru dan map 6.74 dalam jangka waktu dekat ini akan ditetapkan sebagai stabel map atau yang sekarang dikenal dengan Tournament map. Dengan ditetapkannya 6.74 sebagai tournament map, ini akan mempermudah kerja PBMN, cloud_str, dkk, untuk mengerjakan map 6.74 AI, karena map tersebut sudah stabil (tidak memiliki banyak bug).</p>\r\n\r\n<p>Dibawah ini kutipan perkataan PBMN, langsung dari web-nya :</p>\r\n\r\nPBMN said<br>\r\n\r\n<p>Hello.<br>\r\nBy looking at the changes made for 6.74 DotA release, we decided to jump directly to that release. All heroes Item Builds are done, the new item build system is in place and things looks like working smoothly. Well there maybe some unusual stuff going on from time to time.. but that`s considered normal. The new items system tries to mimic human behavior. Builds are being built by stages: starting, core, luxury stage. After core, AI picks up random luxury items... more variations. Cloud_str, Green, gaibo and Simple went to hell and back making that system working.</p>\r\n\r\n<p>Well, release date is close... if nothing crucible happens after 6.74 porting ofc. Stay tuned, wait is almost over.</p><br>\r\n\r\nPBMN', 121, '2012-11-19 21:39:35', '1.jpg', ''),
('2', 'DotA2 vs Blizzard DotA', '<p>Kehadiran Blizzard Dota memang cukup mengguncang para penggemar DOTA.  Apalagi, Valve yang merupakan salah satu pengembang game dari negara yang sama juga menghadirkan DOTA 2. Seperti yang kita ketahui beberapa waktu lalu, Valve telah merangkul pembuat mod Dota yaitu Icefrog. Untuk memperkuat basis fansnya Valve juga mengadakan kompetisi Dota 2 dengan hadiah jutaan dollar.</p>\r\n\r\n<p>Sebagai pengembang game yang memilki jumlah fans yang sangat besar, Blizzard sangat percaya diri untuk menghadirkan game baru ini. Namun apakah dengan hadirnya Blizzard Dota mereka sanggup menarik fans Dota lainnya, yang sudah jatuh cinta dengan DOTA versi valve atau DOTA 2. Namun, kesemua itu tergantung dari keseriusan para pengembangnya untuk memperkuat basis fansnya.</p>\r\n\r\n<p>Jadi bagaimana dengan Anda sendri, mana yang menurut Anda lebih menarik Blizzard DOTA atau DOTA 2?</p>', 31, '2012-11-19 21:37:18', 'blizz.jpg', ''),
('3', 'Secondhand Serenade', '<p>Secondhand Serenade akan kembali menggelar konser di Bandung pada 27 April 2012 di outdoor venue Bandung Convention Center. Konser John cs kali ini dipersembahkan oleh Mahana Live bertepatan dengan launching album terbaru dari Secondhand Serenade.</p>\r\n<p>\r\nBuat yang mau beli tiket konser Secondhand Serenade bisa langsung datang ke:<br>\r\nOfficial Ticket Box Secondhand Serenade Bandung<br>\r\nInfobdg<br>\r\nJl. Setiabudi No. 84 - Lantai 2\r\nBandung<br>\r\n(Depan Hoka-hoka bento, masuk dari samping Millenium karaoke)<br>\r\nTelp: 022-76547474<br>\r\n</p>\r\nHarga Tiket:<br>\r\nRp 220.000', 45, '2012-11-19 21:39:26', 'ss bdg.jpg', ''),
('4', 'The Raid', '<p>Pemain : Iko Uwais, Yayan Ruhian, Donny Alamsyah, C. Joe Taslim Tegar \r\n\r\nSatrya, Pierre Gruno, Ray Sahetapy</p>\r\n\r\n<p>Dimulai dengan adegan pagi hari yang sejuk, Rama(Iko Uwais) beranjak dari \r\n\r\ntempat tidurnya, subuh dan mencium kening istrinya yang sedang hamil. Pagi \r\n\r\nbuta, sebagai salah satu anggota SWAT paling baru, Rama harus bersiap untuk \r\n\r\nmelakukan serangan.</p>\r\n\r\n<p>Bersama dengan tim SWAT, Rama tiba di sebuah blok apartemen yang tidak \r\n\r\nterurus dengan misi menangkap pemiliknya seorang raja bandar narkotik \r\n\r\nbernama Tama (Ray Sahetapy). Blok ini tidak pernah digerebek atau pun \r\n\r\ntersentuh oleh polisi sebelumnya.</p>\r\n\r\n<p>Sebagai tempat yang tidak dijangkau oleh pihak berwajib, gedung tersebut \r\n\r\nmenjadi tempat berlindung para pembunuh, anggota geng, pemerkosa, dan \r\n\r\npencuri yang mencari tempat tinggal aman.</p>\r\n\r\n<p>Kelompok SWAT diam-diam merambah ke dalam gedung dan mengendalikan setiap \r\n\r\nlantai yang mereka naiki dengan mantap. Tetapi ketika mereka terlihat oleh \r\n\r\npengintai Tama, penyerangan mereka terbongkar. Dari penthouse suite-nya, \r\n\r\nTama menginstruksikan untuk mengunci gedung apartemen dengan memadamkan \r\n\r\nlampu dan menutup semua jalan keluar. Bahkan untuk memburu semua pasukan \r\n\r\nSWAT, Tama memberikan bonus bebas biaya sewa untuk siapapun yang berhasil \r\n\r\nmembantai pasukan SWAT. Pertempuran semakin sengit di lantai 6 saat \r\n\r\nkomunikasi pun terputus antar pasukan.</p>\r\n\r\n<p>Terjebak di lantai 6 tanpa komunikasi dan diserang oleh penghuni \r\n\r\napartemen yang diperintahkan oleh Tama, tim SWAT harus berjuang melewati \r\n\r\nsetiap lantai dan setiap ruangan untuk menyelesaikan misi mereka dan \r\n\r\nbertahan hidup. Tidak bisa meminta bantuan dari kesatuan dan tidak ada jalan \r\n\r\nkeluar kecuali menemui dan membunuh Tama di lantai teratas. Tama yang terus \r\n\r\nmemonitor pertempuran di dalam apartemennya, membuat jebakan-jebakan untuk \r\n\r\nmenghambat laju pasukan SWAT.</p>\r\n\r\n<p>Keinginan bertahan hidup, membuat Rama terus melaju mengalahkan lawan-\r\n\r\nlawannya. Dari satu lantai ke lantai yang lain, Rama bertemu dengan musuh \r\n\r\nyang berbeda-beda namun sama bengisnya. Sampai saat bertemu dengan Tama, \r\n\r\nRama dibenturkan dengan ajudan setia Rama, Macdog (Yayan Ruhian). Berjuang \r\n\r\nsampai titik darah penghabisan, Rama menggunakan semua kemampuan \r\n\r\nbeladirinya.</p>\r\n\r\n<p>Dari awal hingga akhir film ini tidak pernah lepas dari ketegangan. Ritme \r\n\r\nfilm yang cepat menambah ketegangan. Dengan gambar yang cenderung gelap dan \r\n\r\ndetil, suasana mencekam akan membuat anda penasaran hingga akhir film.</p>\r\n\r\n<p>Genre	:	Action<br>\r\nJadwal Tayang	:	23 Maret 2012<br>\r\nSutradara	:	Gareth Huw Evans<br>\r\nProduser	:	Ario Sagantoro<br>\r\nProduksi	:	PT. MERANTAU FILMS<br>\r\nDurasi	:	-</p>', 24, '2012-11-19 21:37:40', 'raid.jpg', ''),
('5', 'sssssssss', 'sssssssssssss', 3, '2012-11-22 21:10:29', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `detail_polling`
--

CREATE TABLE IF NOT EXISTS `detail_polling` (
  `id_detail` int(10) NOT NULL AUTO_INCREMENT,
  `id_poll` int(10) DEFAULT NULL,
  `isi_detail` varchar(50) DEFAULT NULL,
  `pemilih` int(10) DEFAULT '0',
  PRIMARY KEY (`id_detail`),
  KEY `detail_polling` (`id_poll`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `detail_polling`
--

INSERT INTO `detail_polling` (`id_detail`, `id_poll`, `isi_detail`, `pemilih`) VALUES
(1, 8, 'opsi 1', 3),
(3, 8, 'opsi 2', 0),
(4, 8, 'opsi 3', 0);

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE IF NOT EXISTS `komentar` (
  `id_komentar` int(10) NOT NULL AUTO_INCREMENT,
  `id_artikel` varchar(5) DEFAULT NULL,
  `nama_komentar` varchar(25) DEFAULT NULL,
  `email_komentar` varchar(50) DEFAULT NULL,
  `isi_komentar` text,
  `tanggal_komentar` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_komentar`),
  KEY `id_artikel` (`id_artikel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id_komentar`, `id_artikel`, `nama_komentar`, `email_komentar`, `isi_komentar`, `tanggal_komentar`) VALUES
(1, '1', 'Anzar', 'zarchp@gmail.com', 'tes tes tes', '2012-11-20 18:33:32'),
(2, '1', 'zar', 'aaaa', 'GG', '2012-11-20 19:02:40'),
(4, '2', 'anzar', '', 'dota 2', '2012-11-20 19:11:54'),
(6, '1', 'fajar', '', 'anzar gg', '2012-11-21 07:30:00'),
(7, '1', 'tiswan', '', 'anzar tukang sampah!!\r\n', '2012-11-21 10:31:26');

-- --------------------------------------------------------

--
-- Table structure for table `polling`
--

CREATE TABLE IF NOT EXISTS `polling` (
  `id_poll` int(10) NOT NULL AUTO_INCREMENT,
  `judul_poll` varchar(50) DEFAULT NULL,
  `deskripsi` text,
  PRIMARY KEY (`id_poll`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `polling`
--

INSERT INTO `polling` (`id_poll`, `judul_poll`, `deskripsi`) VALUES
(8, 'polling1', 'contoh');

-- --------------------------------------------------------

--
-- Table structure for table `view`
--

CREATE TABLE IF NOT EXISTS `view` (
  `id` varchar(10) NOT NULL,
  `views` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `view`
--

INSERT INTO `view` (`id`, `views`) VALUES
('1', 339);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_polling`
--
ALTER TABLE `detail_polling`
  ADD CONSTRAINT `detail_polling_ibfk_1` FOREIGN KEY (`id_poll`) REFERENCES `polling` (`id_poll`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `komentar`
--
ALTER TABLE `komentar`
  ADD CONSTRAINT `komentar_ibfk_1` FOREIGN KEY (`id_artikel`) REFERENCES `artikel` (`id_artikel`) ON DELETE CASCADE ON UPDATE CASCADE;
